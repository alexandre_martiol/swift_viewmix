//
//  CollectionViewController2.swift
//  ViewMix
//
//  Created by AlexM on 22/01/15.
//  Copyright (c) 2015 AlexM. All rights reserved.
//

import UIKit

class CollectionViewController2: UICollectionViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    var imgToShow = [UIImage (named: "anonymous1.png")!, UIImage (named: "anonymous2.png")!, UIImage (named: "anonymous3.png")!]
    var imgNames = ["anonymous1.png", "anonymous2.png","anonymous3.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "CollectionCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return 40
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //Crea una variable cell de tipo UICollectionViewCell con el método dequeueReusableCellWithReuseIdentifier del collectionView.
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionCell2", forIndexPath: indexPath) as UICollectionViewCell
        
        //Crea una UIImageView usando el método cell.viewWithTag(1)
        var imageView = cell.viewWithTag(1) as UIImageView
        
        //Añade una imagen al UIImageView en función del indexPath. Usa el NSArray de imágenes que habías creado.
        imageView.image = imgToShow[indexPath.row % 3]
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
            var imagenSeleccionada = imgNames[indexPath.row % 3]
            
            var vistaDetalle: DetailViewController2 = self.storyboard?.instantiateViewControllerWithIdentifier("Detalle2") as DetailViewController2
            vistaDetalle.stringImage = imagenSeleccionada
            self.navigationController?.pushViewController(vistaDetalle, animated: true)
    }

}






