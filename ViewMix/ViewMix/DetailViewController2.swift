//
//  DetailViewController2.swift
//  ViewMix
//
//  Created by AlexM on 22/01/15.
//  Copyright (c) 2015 AlexM. All rights reserved.
//

import UIKit

class DetailViewController2: UIViewController {

    @IBOutlet weak var viewDetail2: UIImageView!
    var stringImage = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewDetail2.image = UIImage (named: stringImage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
